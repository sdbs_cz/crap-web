function animateVolume (el, duration, from, to) {
    return new Promise((resolve) => {
        const start = new Date();
        const volumeInterval = setInterval(() => {
            const delta = new Date().getTime() - start.getTime();
            if (delta > duration) {
                clearInterval(volumeInterval);
                resolve();
            } else {
                el.volume = delta / duration * (to - from) + from;
            }
        }, 1000 / 30);
    });
}

const choices = ["slitscan_a.webm", "slitscan_b.webm", "slitscan_c.webm", "virus_web.webm"];
let lastVideos = [];
let playedVideos = 0;
const LAST_N = 2;

function chooseNextVideoSource () {
    let viableChoices = choices;
    if (playedVideos < 5) {
        viableChoices = viableChoices.filter((choice) => choice.includes("slitscan"));
    }
    viableChoices = viableChoices.filter((choice) => !lastVideos.includes(choice));
    const result = viableChoices[Math.floor(Math.random() * viableChoices.length)];
    lastVideos.push(result);
    if (lastVideos.length > LAST_N) {
        lastVideos.shift();
    }
    playedVideos += 1;
    return result;
}

let A_TO_B = true;
let switchTimeout;

document.getElementById("button-start").addEventListener("click", () => {
    const presentation = document.getElementById("presentation");
    const videoA = document.getElementById("presentation-video-a");
    const videoB = document.getElementById("presentation-video-b");
    const audio = document.getElementById("presentation-audio");
    const veil = document.getElementById("presentation-veil");

    presentation.style.display = "block";
    presentation.requestFullscreen().then(() => {
        const IN_DURATION = 6969;
        videoA.src = `assets/retransmission/${chooseNextVideoSource()}`;
        videoA.load();
        videoA.play();
        audio.play();
        animateVolume(audio, IN_DURATION, 0, 1);


        veil.animate(
            [
                {background: "black", opacity: 1},
                {background: "white", opacity: 1},
                {background: "white", opacity: 0}
            ], {duration: IN_DURATION, fill: "forwards", easing: "ease-in-out"});

        presentation.addEventListener("fullscreenchange", () => {
            if (document.fullscreenElement === null) {
                clearTimeout(switchTimeout);
                presentation.style.display = "none";
                videoA.pause();
                animateVolume(audio, 777, 1, 0).then(() => {
                    audio.pause();
                });
            }
        });

        function nextVideo () {
            const XFADE_DURATION = 1000;

            const inEl = A_TO_B ? videoB : videoA;
            const outEl = A_TO_B ? videoA : videoB;

            A_TO_B = !A_TO_B;

            const source = `assets/retransmission/${chooseNextVideoSource()}`;
            inEl.src = source;
            console.debug(`Switching to ${source}...`);
            inEl.load();
            inEl.play();

            inEl.onloadeddata = () => {
                console.debug(`Resizing video:${inEl.src}: ${inEl.videoWidth}x${inEl.videoHeight}`);
                inEl.style["width"] = `${100 * inEl.videoWidth / inEl.videoHeight}vh`;
                inEl.style["min-height"] = `${100 * inEl.videoHeight / inEl.videoWidth}vw`;
            };

            inEl.ondurationchange = () => {
                if (!isNaN(inEl.duration) && inEl.duration !== Infinity) {
                    inEl.ondurationchange = undefined;

                    const animOpts = {duration: XFADE_DURATION, fill: "forwards", easing: "ease-in-out"};
                    inEl.animate([
                        {opacity: 0},
                        {opacity: 1}
                    ], animOpts);
                    outEl.animate([
                        {opacity: 1},
                        {opacity: 0}
                    ], animOpts);

                    setTimeout(() => {
                        outEl.pause();
                    }, XFADE_DURATION + 100);

                    const timeout = inEl.duration > 60 ?
                        inEl.duration * 1000 - XFADE_DURATION :
                        Math.max(XFADE_DURATION * 2, inEl.duration * 1000 * 3,
                            Math.random() * 23_937,
                            playedVideos * 1000 * 1.5 + (Math.random() * 2 - 1) * (playedVideos * 1000 * .33));

                    console.debug(`Next switch in ${timeout}ms...`);
                    clearTimeout(switchTimeout);
                    switchTimeout = setTimeout(() => {
                        nextVideo();
                    }, timeout);
                }
            };
        }

        setTimeout(() => {
            nextVideo();
        }, IN_DURATION + IN_DURATION / 2 + Math.random() * IN_DURATION * 1.5);


    }).catch((error) => {
        alert(error);
    });
});
