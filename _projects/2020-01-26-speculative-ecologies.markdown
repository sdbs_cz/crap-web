---
title: Speculative Ecologies
cover: speculative-ecologies.png
date: 2020-01-26
layout: post
---
![](/assets/speculative-ecologies-flier.jpg)
## Speculative Ecologies Launch and Podcast

[**PUNCTUM**](https://punctum.cz), [Krásova 803/27, 130 00 Praha 3 – Žižkov](https://goo.gl/maps/Hcj39jRZNdrv8o4b9)

We would cordially like to invite you to the launch of the essay collection _Speculative Ecologies: Plotting Through the Mesh_ ([Litteraria Pragensia](http://litteraria-pragensia.ff.cuni.cz) Books, 2019), featuring **lectures** and a **live podcast**.  

The evening will be conceived as **an open session** discussing the topics provoked by the book, namely the ongoing specter of climate change, the role of art and making for the mitigation of its effects, and the position of contemporary philosophical theory on the environmental problems (and solutions) at hand. 

The schedule is as follows:

17.00 – Introduction to _Speculative Ecologies_

17.10 – Kateřina Kovářová: Technology at the Service of Environmental Ethics

18.30 – [Paul Chaney](https://twitter.com/paulchny): Towards Appropriate Accelerationism?

20.00 – Panel discussion with Louis Armand, Paul Chaney, Seb Chum, [Xenogoth](https://twitter.com/xenogothic), [@baroquespiral](https://twitter.com/baroquespiral) and the audience   

21.00 – sounds by TL;DR 

Moderator: Vít Bohal

The event is kindly supported by the Agosto Foundation, who are also launching their ongoing series **_Speculative Ecologies: The Fertile Ground Between Art and Ecology_**as part of their media library. 

Please visit [https://agosto-foundation.org/mediateka/speculative-ecologies](https://agosto-foundation.org/mediateka/speculative-ecologies)

Free admission.

The event will be mostly held in English.

* * *

## Speculative Ecologies: Křest a Podcast

[**PUNCTUM**](https://punctum.cz), [Krásova 803/27, 130 00 Praha 3 – Žižkov](https://goo.gl/maps/Hcj39jRZNdrv8o4b9)

Dovolujeme si vás pozvat na křest sbírky esejů _Speculative Ecologies: Plotting Through the Mesh_ ([Litteraria Pragensia](http://litteraria-pragensia.ff.cuni.cz) Books, 2019), a s ním spojené **přednášky** a **živý podcast**.

Večer bude volně navazovat a řešit témata knihy, konkrétně stle se zhoršující stav globálního klimatu, role umění a tvorby pro zmírnění jeho dopadů, a přístup současné filozofie a teorie k těmto environmentálním problémům dneška.

Rozvrh:

17.00 – Úvod do Spekulativní ekologie

17.10 – Kateřina Kovářová: Technology at the Service of Environmental Ethics

18.30 – Paul Chaney: Towards Appropriate Accelerationism?

20.00 – Panelová diskuze: Louis Armand, Paul Chaney, Seb Chum, Xenogoth, @baroquespiral a obecenstvo   

21.00 – zvuky: TL;DR 

Moderátor: Vít Bohal

Večer se koná za podpory Nadace Agosto Foundation, která současně bude křtít novou část své mediatéky zvanou **_Speculative Ecologies: Umělecké spekulace s ekologickým myšlením. _**

Navštivte: [https://agosto-foundation.org/cs/mediateka/speculative-ecologies](https://agosto-foundation.org/cs/mediateka/speculative-ecologies)

Vstup zdarma.

Večer bude veden převážně v angličtině.
