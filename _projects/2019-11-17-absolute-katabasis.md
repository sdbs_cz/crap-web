---
title: "Miroslav Griško: Absolute Katabasis"
cover: absolute-katabasis.jpg
date: 2019-11-17
layout: post
---
Diffractions Collective (Dustin Breitling and Vit Van Camp) and /-\\ (Sebastian Chum & and our gratitude to Tomáš Mládek for recording) interview Miroslav Griško (Collaborator with Slovenian based ŠUM Journal) about teleology, war, and eschatology. Our gratitude also extends to [Punctum](https://punctum.cz) that provided the venue, equipment and hospitality.

<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/716901736&color=%23ff0000&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe>

